package vafli.mrp_test;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Q on 22.03.2015.
 */
public class CustomMaskView extends View {

    public static final int CORNERS_RADIUS = 15;

    private static final int MARGIN_X = 50;
    private static final double MASK_RATIO = 233f / 1243f;

    private Paint mPaint;
    private RectF maskRect;

    private int width;
    private int height;

    public CustomMaskView(Context context) {
        super(context);
        init();
    }

    public CustomMaskView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setColor(Color.rgb(0, 255, 0));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(3);

        maskRect = new RectF();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = MeasureSpec.getSize(widthMeasureSpec);
        height = MeasureSpec.getSize(heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int heightCenter = height / 2;

        int maskWidth = width - 2 * MARGIN_X;
        int maskHeight = (int) (maskWidth * MASK_RATIO);
        int maskY = heightCenter - (maskHeight / 2);
        maskRect.set(MARGIN_X, maskY,
                MARGIN_X + maskWidth, maskY + maskHeight);

        canvas.drawRoundRect(maskRect, CORNERS_RADIUS, CORNERS_RADIUS, mPaint);
    }

    public RectF getMaskRect() {
        return maskRect;
    }
}