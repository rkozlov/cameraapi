package vafli.mrp_test;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Q on 22.03.2015.
 */
public class ImageUtil {

    private static final String TAG = ImageUtil.class.getSimpleName();

    public static Bitmap getImageBitmap(byte[] data, CustomMaskView view) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        Log.i(TAG, "bitmap width = " + bitmap.getWidth() + " height = " + bitmap.getHeight());
        Log.i(TAG, "view width = " + view.getWidth() + " height = " + view.getHeight());
        Log.i(TAG, "rect width = " + view.getMaskRect().width() + " height = " + view.getMaskRect().height());

        int scaleWidth = (int) (view.getMaskRect().width() * bitmap.getWidth() / view.getWidth());
        int scaleHeight = (int) (view.getMaskRect().height() * bitmap.getHeight() / view.getHeight());
        int scaleX = (int) (view.getMaskRect().left * bitmap.getWidth() / view.getWidth());
        int scaleY = (int) (view.getMaskRect().top * bitmap.getHeight() / view.getHeight());

        Log.i(TAG, "scale width = " + scaleWidth + " scaleHeight = " + scaleHeight + " scaleLeft = " + scaleX + " scale Y = " + scaleY);

        int[] pixels = new int[scaleWidth * scaleHeight];
        bitmap.getPixels(pixels, 0, scaleWidth, scaleX, scaleY, scaleWidth, scaleHeight);
        bitmap = Bitmap.createBitmap(pixels, 0, scaleWidth, scaleWidth, scaleHeight, Bitmap.Config.ARGB_8888);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] mrp = bos.toByteArray();

        Bitmap cropBitmap = BitmapFactory.decodeByteArray(mrp, 0, mrp.length);
        Log.i(TAG, "cropBitmap width = " + cropBitmap.getWidth() + " height = " + cropBitmap.getHeight());
        return cropBitmap;
    }
}
